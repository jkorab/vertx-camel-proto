package com.ameliant.proto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VertxCamelProtoApplication {

	public static void main(String[] args) {
		SpringApplication.run(VertxCamelProtoApplication.class, args);
	}
}
